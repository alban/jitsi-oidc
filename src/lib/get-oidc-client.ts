import { Issuer, Client } from "openid-client";
import { oidcIssuer, oidcClientId, oidcClientSecret } from "../config";

export async function getOIDCClient(): Promise<Client> {
  const issuer = await Issuer.discover(oidcIssuer);

  return new issuer.Client({
    client_id: oidcClientId,
    client_secret: oidcClientSecret,
    response_types: ["code"],
  });
}
