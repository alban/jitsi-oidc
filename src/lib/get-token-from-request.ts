import { Request } from "express";

export function getTokenFromRequest(req: Request): string | null {
  if (typeof req.query.jwt !== "string") {
    return null;
  }

  return req.query.jwt;
}
