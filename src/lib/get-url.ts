import { Request } from "express";

export function getUrl(req: Request) {
  const host = req.headers.host || req.hostname;
  const proto = req.headers["X-Forwarded-Proto"] || req.protocol;
  const path = req.path;

  return `${proto}://${host}${path}`;
}
