import { Request } from "express";

export function getBaseUrl(req: Request) {
  const host = req.headers.host || req.hostname;
  const proto = req.headers["X-Forwarded-Proto"] || req.protocol;

  return `${proto}://${host}`;
}
