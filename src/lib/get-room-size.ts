import fetch from "node-fetch";
import {
  enableMUCSize,
  xmppDomain,
  xmppServer,
  xmppServerPort,
} from "../config";

export async function getRoomSize(room: string): Promise<number> {
  if (!enableMUCSize) {
    return 0;
  }

  try {
    const data = await fetch(
      `http://${xmppServer}:${xmppServerPort}/room-size?room=${room}&domain=${xmppDomain}`
    ).then((r) => r.json());

    if (!Number.isInteger(data.participants)) {
      return 0;
    }

    return data.participants;
  } catch (e) {
    return 0;
  }
}
