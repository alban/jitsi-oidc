import jwt from "jsonwebtoken";
import { IdTokenClaims } from "openid-client";

import { jitsiSecret, jitsiSub, jitsiIssuer, jitsiAudience } from "../config";

export function createToken(
  allowedRoom: string,
  claims?: IdTokenClaims
): string {
  const context: any = {};

  if (claims) {
    context.user = {};

    if (claims.email) {
      context.user.email = claims.email;
    }

    if (claims.name) {
      context.user.name = claims.name;
    } else {
      context.user.name = claims.preferred_username;
    }
  }

  return jwt.sign(
    {
      context,
      aud: jitsiAudience,
      iss: jitsiIssuer,
      sub: jitsiSub,
      room: allowedRoom,
      moderator: !!claims,
    },
    jitsiSecret,
    {
      expiresIn: 30,
    }
  );
}
