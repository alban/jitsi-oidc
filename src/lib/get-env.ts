export function getEnv(key: string, defaultValue?: string): string {
  const value: string | undefined = process.env[key];
  if (typeof value === "string") {
    return value;
  }

  if (typeof defaultValue === "string") {
    console.log(
      `Environment variable ${key} not defined. Using default: ${defaultValue}`
    );
    return defaultValue;
  }

  throw new Error(`Environment variable ${key} not defined.`);
}
