import jwt from "jsonwebtoken";

import { jitsiSecret, jitsiSub, jitsiIssuer, jitsiAudience } from "../config";

export function verifyToken(token: string, room: string): boolean {
  try {
    const payload: any = jwt.verify(token, jitsiSecret, {
      audience: jitsiAudience,
      issuer: jitsiIssuer,
      subject: jitsiSub,
    });

    if (typeof payload === "string") {
      return false;
    }

    if (![room, "*"].includes(payload.room)) {
      return false;
    }

    return true;
  } catch (e) {
    return false;
  }
}
