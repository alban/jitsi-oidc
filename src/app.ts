import express, { Express, Request, Response } from "express";
import { allowGuestRooms, enableGuestTokens } from "./config";
import {
  createToken,
  getBaseUrl,
  getOIDCClient,
  getRoomSize,
  getTokenFromRequest,
  verifyToken,
} from "./lib";

const app: Express = express();

app.get("/validate/*", (req: Request, res: Response) => {
  const room = req.params[0];
  const token = getTokenFromRequest(req);

  if (!token) {
    res.sendStatus(401);
    return;
  }

  if (!verifyToken(token, room)) {
    res.sendStatus(401);
    return;
  }

  res.sendStatus(200);
});

app.get(
  "/authorize/*",
  async (req: Request, res: Response): Promise<void> => {
    const room = req.params[0];
    const roomSize = await getRoomSize(room);
    const url = `${getBaseUrl(req)}/${room}`;

    if (
      req.query.error &&
      enableGuestTokens &&
      (roomSize > 0 || allowGuestRooms)
    ) {
      // redirect with guest token
      const token = createToken(room);
      return res.redirect(url + "?jwt=" + token);
    }

    const client = await getOIDCClient();

    if (req.query.code) {
      try {
        const params = client.callbackParams(req);
        const tokenSet = await client.callback(url, params);
        const token = createToken(room, tokenSet.claims());

        return res.redirect(url + "?jwt=" + token);
      } catch (e) {
        console.error(e);
        return res.redirect("/static/authError.html");
      }
    }

    if ((roomSize === 0 || !enableGuestTokens) && !allowGuestRooms) {
      // login prompt
      return res.redirect(
        client.authorizationUrl({
          redirect_uri: url,
          scope: "openid email profile",
        })
      );
    } else {
      // silent login
      return res.redirect(
        client.authorizationUrl({
          redirect_uri: url,
          scope: "openid email profile",
          prompt: "none",
        })
      );
    }
  }
);

export default app;
