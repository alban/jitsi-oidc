import { getEnv } from "./lib";

export const jitsiSecret: string = getEnv("JWT_APP_SECRET");

export const jitsiSub: string = getEnv("JWT_APP_ID");

export const jitsiIssuer: string = getEnv("JWT_ISSUER");

export const jitsiAudience: string = getEnv("JWT_AUDIENCE");

export const enableMUCSize: boolean = getEnv("ENABLE_MUC_SIZE", "0") === "1";

export const xmppDomain: string = getEnv("XMPP_DOMAIN", "meet.jitsi");

export const xmppServer: string = getEnv("XMPP_SERVER", "xmpp.meet.jitsi");

export const xmppServerPort: number = Number(
  getEnv("XMPP_SERVER_PORT", "5280")
);

export const enableGuestTokens: boolean =
  getEnv("ENABLE_GUEST_TOKENS", "0") === "1";

export const allowGuestRooms: boolean =
  getEnv("ALLOW_GUEST_ROOMS", "0") === "1" && enableGuestTokens;

export const oidcIssuer: string = getEnv("OIDC_ISSUER");

export const oidcClientId: string = getEnv("OIDC_CLIENT_ID");

export const oidcClientSecret: string = getEnv("OIDC_CLIENT_SECRET");
