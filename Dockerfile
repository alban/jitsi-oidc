FROM node:12-alpine as server-build

WORKDIR /usr/src/app
COPY . .
RUN npm ci -q
RUN npm run -s build
RUN npm prune -s --production

FROM node:12-alpine

WORKDIR /usr/src/app
COPY --from=server-build /usr/src/app/node_modules ./node_modules
COPY --from=server-build /usr/src/app/dist/ ./

EXPOSE 3000

CMD [ "node", "server.js" ]